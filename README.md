# e3-dtlhighlevel

Wrapper for the module dtlhighlevel. This is a sequencer-based module that
provides a state machine for the DTL tanks.

A simple description of the possible states of the individual DTL tanks is

![states](docs/states.png)