%{

static void printTSMsg(int debug_lvl, const char * fmt, ...) {
    if (debug_lvl <= DEBUG) {
        char buffer[4096];
        va_list args;
        va_start(args, fmt);
        vsprintf(buffer, fmt, args);
        va_end(args);
        epicsTimeStamp now_ts;
        epicsTimeGetCurrent( &now_ts);
        char nowText[40];
        nowText[0] = 0;
        epicsTimeToStrftime(nowText,sizeof(nowText),"%Y/%m/%d %H:%M:%S.%03f",&now_ts);
        printf("%s:: %s", nowText,buffer);
    }
 }

}%
